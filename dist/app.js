const DB = {
  header: "tiagolisalves",
  nav: { links: [{ href: "blog", label: "Blog" }, { href: "aboutme", label: "About me" }] }
};
const editor = document.querySelector("#editor");

const renderEditor = () => {
  editor.innerHTML = html`
    <div class="editor-area">
      <textarea id="markup-editor" cols="30" class="editor" rows="10"></textarea>
      <div id="markup-layout"></div>
    </div>
  `;
};

const clearContent = () => {
  editor.innerHTML = html`
    <div></div>
  `;
};

window.addEventListener("popstate", event => {
  if (location.pathname === "/event") {
    renderEditor();
  } else {
    clearContent();
  }
});
var app = document.querySelector("#app");

var navLinks = DB.nav.links
  .map(function(link) {
    return `
      <a href="${link.href}" class="nav-link">${link.label}</a>
      `;
  })
  .join("\n");
var mark = function(mdText) {
  return marked(mdText);
};
var posts = DB.posts
  .map(function(post) {
    return html`
      <div class="post">
        <div class="post-title">${post.title}</div>
        <div class="post-content">${post.marked === false ? post.content : mark(post.content)}</div>
      </div>
    `;
  })
  .join("\n");

const renderApp = () => {
  app.innerHTML = html`
    <header>${DB.header}</header>
    <nav>
      ${navLinks}
    </nav>
    <main>
      <div class="post-area">
        ${posts}
      </div>
    </main>
  `;
};

window.addEventListener("popstate", () => {
  if (location.pathname === "/") {
    renderApp();
  }
});
