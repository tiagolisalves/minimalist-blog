"use strict";

var _templateObject = _taggedTemplateLiteral(["\n    <div class=\"editor-area\">\n      <textarea id=\"markup-editor\" cols=\"30\" class=\"editor\" rows=\"10\"></textarea>\n      <div id=\"markup-layout\"></div>\n    </div>\n  "], ["\n    <div class=\"editor-area\">\n      <textarea id=\"markup-editor\" cols=\"30\" class=\"editor\" rows=\"10\"></textarea>\n      <div id=\"markup-layout\"></div>\n    </div>\n  "]),
    _templateObject2 = _taggedTemplateLiteral(["\n    <div></div>\n  "], ["\n    <div></div>\n  "]);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var editor = document.querySelector("#editor");

var renderEditor = function renderEditor() {
  editor.innerHTML = html(_templateObject);
};

var clearContent = function clearContent() {
  editor.innerHTML = html(_templateObject2);
};

window.addEventListener("popstate", function (event) {
  if (location.pathname === "/event") {
    renderEditor();
  } else {
    clearContent();
  }
});