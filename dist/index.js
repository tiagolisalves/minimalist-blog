"use strict";

var _templateObject = _taggedTemplateLiteral(["\n      <div class=\"post\">\n        <div class=\"post-title\">", "</div>\n        <div class=\"post-content\">", "</div>\n      </div>\n    "], ["\n      <div class=\"post\">\n        <div class=\"post-title\">", "</div>\n        <div class=\"post-content\">", "</div>\n      </div>\n    "]),
    _templateObject2 = _taggedTemplateLiteral(["\n    <header>", "</header>\n    <nav>\n      ", "\n    </nav>\n    <main>\n      <div class=\"post-area\">\n        ", "\n      </div>\n    </main>\n  "], ["\n    <header>", "</header>\n    <nav>\n      ", "\n    </nav>\n    <main>\n      <div class=\"post-area\">\n        ", "\n      </div>\n    </main>\n  "]);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var app = document.querySelector("#app");

var navLinks = DB.nav.links.map(function (link) {
  return "\n      <a href=\"" + link.href + "\" class=\"nav-link\">" + link.label + "</a>\n      ";
}).join("\n");
var mark = function mark(mdText) {
  return marked(mdText);
};
var posts = DB.posts.map(function (post) {
  return html(_templateObject, post.title, post.marked === false ? post.content : mark(post.content));
}).join("\n");

var renderApp = function renderApp() {
  app.innerHTML = html(_templateObject2, DB.header, navLinks, posts);
};

window.addEventListener("popstate", function () {
  if (location.pathname === "/") {
    renderApp();
  }
});