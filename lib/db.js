"use strict";

var DB = {
  header: "tiagolisalves",
  nav: { links: [{ href: "blog", label: "Blog" }, { href: "aboutme", label: "About me" }] },
  posts: [{
    title: "My first post",
    content: "\n      Hello, this is the first post of this blog. \n      Here I want to share with you the things that I've learned in my Software Developer journey. \n      The motivation I have is due to other blogs in the internet that helped me so much. \n      It's a way to retribute. After many hello worlds I will create mine:\n\n      function hello(){\n        console.log(\"Hello World\")\n      }\n      hello()\n    "
  }]
};