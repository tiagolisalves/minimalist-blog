"use strict";

var app = document.querySelector("#app");

var navLinks = DB.nav.links.map(function (link) {
  return "\n      <a href=\"" + link.href + "\" class=\"nav-link\">" + link.label + "</a>\n    ";
}).join("\n");
var mark = function mark(_ref) {
  var mdText = _ref.mdText;

  var transformedHTML = marked(mdText);
  return transformedHTML;
};
var posts = DB.posts.map(function (post) {
  return "\n      <div class=\"post\">\n        <div class=\"post-title\">" + post.title + "</div>\n        <div class=\"post-content\">" + (post.marked === false ? post.content : mark({ mdText: post.content })) + "</div>\n      </div>\n    ";
}).join("\n");

app.innerHTML = "\n  <header>" + DB.header + "</header>\n  <nav>\n    " + navLinks + "\n  </nav>\n  <main>\n    <div class=\"post-area\">\n      " + posts + "\n    </div>\n  </main>\n";