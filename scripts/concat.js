const glob = require("glob");
const argv = require("yargs").argv;
const fs = require("fs");
const path = require("path");

const getSortedFilePaths = patterns =>
  patterns.sort((a, b) => {
    const key = a => a.substring(a.lastIndexOf("\\"), a.lastIndexOf("."));
    if (key(a) > key(b)) return 1;
    if (key(a) < key(b)) return -1;
    return 0;
  });
const filePaths = glob.sync(argv.p, { cwd: path.join(__dirname, "../src") });
let content = "";
getSortedFilePaths(filePaths).forEach(filePath => {
  const fileContent = fs.readFileSync(path.join(__dirname, "../src/" + filePath), "UTF8");

  content += fileContent;
});

fs.writeFileSync(path.join(__dirname, "../" + argv.f), content);
