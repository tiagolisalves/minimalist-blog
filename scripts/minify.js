var jsp = require("uglify-js").parser;
var pro = require("uglify-js").uglify;
const fs = require("fs");
const path = require("path");
const args = require("yargs").argv;
console.log(args);
var orig_code = fs.readFileSync(path.join(__dirname, "../" + args.f), "UTF8");
var ast = jsp.parse(orig_code); // parse code and get the initial AST
ast = pro.ast_mangle(ast); // get a new AST with mangled names
ast = pro.ast_squeeze(ast); // get an AST with compression optimizations
var final_code = pro.gen_code(ast);

fs.writeFileSync(path.join(__dirname, "../" + args.f), final_code);
