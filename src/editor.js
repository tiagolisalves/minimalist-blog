const editor = document.querySelector("#editor");

const renderEditor = () => {
  editor.innerHTML = html`
    <div class="editor-area">
      <textarea id="markup-editor" cols="30" class="editor" rows="10"></textarea>
      <div id="markup-layout"></div>
    </div>
  `;
};

const clearContent = () => {
  editor.innerHTML = html`
    <div></div>
  `;
};

window.addEventListener("popstate", event => {
  if (location.pathname === "/event") {
    renderEditor();
  } else {
    clearContent();
  }
});
