var app = document.querySelector("#app");

var navLinks = DB.nav.links
  .map(function(link) {
    return `
      <a href="${link.href}" class="nav-link">${link.label}</a>
      `;
  })
  .join("\n");
var mark = function(mdText) {
  return marked(mdText);
};
var posts = DB.posts
  .map(function(post) {
    return html`
      <div class="post">
        <div class="post-title">${post.title}</div>
        <div class="post-content">${post.marked === false ? post.content : mark(post.content)}</div>
      </div>
    `;
  })
  .join("\n");

const renderApp = () => {
  app.innerHTML = html`
    <header>${DB.header}</header>
    <nav>
      ${navLinks}
    </nav>
    <main>
      <div class="post-area">
        ${posts}
      </div>
    </main>
  `;
};

window.addEventListener("popstate", () => {
  if (location.pathname === "/") {
    renderApp();
  }
});
